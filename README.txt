Feeds Plupload Fetcher
======================

A feeds fetcher plugin providing Drag&Drop file uploads.

Installation
------------

* Download and install the Plupload integration module
  https://www.drupal.org/project/plupload
* Navigate to Administration » Reports » Status report and verify that
  Plupload is installed correctly.
* Download and install the Feeds Plupload Fetcher module
  https://www.drupal.org/project/feeds_plupload

Configuration
-------------

* Navigate to Administration » Structure » Feeds importers and add a new
  importer or edit an existing one.
* Change the "Fetcher" of the importer to "File upload with Plupload".
