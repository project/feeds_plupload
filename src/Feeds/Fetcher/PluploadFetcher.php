<?php

namespace Drupal\feeds_plupload\Feeds\Fetcher;

use Drupal\Component\Serialization\Json;
use Drupal\feeds\Exception\EmptyFeedException;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResult;
use Drupal\feeds\Result\FetcherResultInterface;
use Drupal\feeds\State;
use Drupal\feeds\StateInterface;

// cspell:ignore autoupload tmppath

/**
 * Fetches data using plupload form element.
 *
 * @FeedsFetcher(
 *   id = "plupload",
 *   title = @Translation("File upload with Plupload"),
 *   description = @Translation("Upload content from local files with the plupload widget."),
 *   form = {
 *     "configuration" = "Drupal\feeds_plupload\Feeds\Fetcher\Form\PluploadFetcherConfigForm",
 *     "feed" = "Drupal\feeds_plupload\Feeds\Fetcher\Form\PluploadFetcherFeedForm",
 *   },
 * )
 */
class PluploadFetcher extends PluginBase implements FetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state): FetcherResultInterface {
    assert($state instanceof State);
    if (!isset($state->files)) {
      // Extract files from plupload element.
      $files = Json::decode($feed->getSource());
      $state->files = is_array($files) ? $files : [];
      $state->total = count($state->files);
    }

    if ($state->files) {
      $file = array_shift($state->files);
      $state->progress($state->total, $state->total - count($state->files));
      return new FetcherResult($file['tmppath']);
    }

    throw new EmptyFeedException();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array{}
   */
  public function defaultFeedConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function defaultConfiguration(): array {
    return [
      'allowed_extensions' => 'txt csv tsv xml opml',
      'autoupload' => FALSE,
      'autosubmit' => FALSE,
    ];
  }

}
