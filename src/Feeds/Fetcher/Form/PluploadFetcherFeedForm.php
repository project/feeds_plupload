<?php

namespace Drupal\feeds_plupload\Feeds\Fetcher\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

// cspell:ignore autoupload

/**
 * Provides a form on the feed edit page for the plupload fetcher.
 */
class PluploadFetcherFeedForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
    ?FeedInterface $feed = NULL,
  ): array {
    $configuration = $this->plugin->getConfiguration();
    $form['source'] = [
      '#type' => 'plupload',
      '#title' => t('Upload files'),
      '#required' => TRUE,
      '#autoupload' => $configuration['autoupload'],
      '#autosubmit' => $configuration['autosubmit'],
      '#upload_validators' => [
        'FileExtension' => [
          'extensions' => $configuration['allowed_extensions'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
    ?FeedInterface $feed = NULL,
  ): void {
    $feed?->setSource(Json::encode($form_state->getValue('source')));
  }

}
