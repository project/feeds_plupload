<?php

namespace Drupal\feeds_plupload\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

// cspell:ignore autoupload

/**
 * The configuration form for the plupload fetcher.
 */
class PluploadFetcherConfigForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ): array {
    $configuration = $this->plugin->getConfiguration();
    $form['allowed_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#description' => $this->t('Allowed file extensions for upload.'),
      '#default_value' => $configuration['allowed_extensions'],
    ];
    $form['autoupload'] = [
      '#type' => 'checkbox',
      '#title' => t('Auto upload'),
      '#description' => t('Start uploading immediately after files are added.'),
      '#default_value' => $configuration['autoupload'],
    ];
    $form['autosubmit'] = [
      '#type' => 'checkbox',
      '#title' => t('Auto submit'),
      '#description' => t('Submit the form after automatic upload has finished.'),
      '#default_value' => $configuration['autosubmit'],
      '#states' => [
        'enabled' => [
          'input[name="autoupload"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $values = &$form_state->getValues();
    $values['allowed_extensions'] = preg_replace('/\s+/', ' ', trim($values['allowed_extensions']));
  }

}
